## Support Ticket Details

### Description
A clear and concise description of the bug.

### Reproduction Steps
1. Step-by-step instructions to reproduce the bug.
2. Include any specific configurations or conditions necessary for reproduction.

### Expected Behavior
A description of what you expected to happen.

### Actual Behavior
A description of what actually happened.

### Screenshots
If applicable, add screenshots to help explain your problem.

### Environment
- **Operating System:**
- **Browser (if applicable):**
- **GitLab Version:**
- **Browser Version (if applicable):**
- **Any other relevant information:**

### Additional Information
Any additional information that may be helpful in resolving the issue.

### Possible Solution
If you have suggestions on how to fix the bug or what might be causing it, please provide them here.

### Related Issues/PRs
List of any related issues or pull requests.


### Labels
/label ~"Issue Type::Support Task"

/assign @hossamhamdy



